//
//  ViewController.m
//  sideMenu
//
//  Created by Click Labs130 on 10/23/15.
//  Copyright (c) 2015 Click Labs130. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UIView *view;
@property (strong, nonatomic) IBOutlet UIView *sideMenu;
@property (strong, nonatomic) IBOutlet UIButton *button;

@end

@implementation ViewController
@synthesize sideMenu;
@synthesize button;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
//    UIView *sideMenu
//    sideMenu.frame=CGRectMake(-220, 0, 247, 667);
//    [self.view addSubview:sideMenu];
    
    
        [button addTarget:self action:@selector(showMenuItem) forControlEvents:UIControlEventTouchUpInside];
    

}
-(void) showMenuItem{
    sideMenu.frame=CGRectMake(0, 0, 247, 667);
    }
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
