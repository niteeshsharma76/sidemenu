//
//  AppDelegate.h
//  sideMenu
//
//  Created by Click Labs130 on 10/23/15.
//  Copyright (c) 2015 Click Labs130. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

